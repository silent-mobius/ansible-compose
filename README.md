# Docker Compose Environment for Learning Ansible

- Project designed to be taught in class or self paced
	- validate that you have dependencies for the project to work:
		- Linux OS (Debian/Rocky)
		- git
		- docker
		- docker-compose
  
- To Use the project:
  - clone this repo with : `git clone https://gitlab.com/silent-mobius/ansible-compose.git`
  - cd into project directory: `cd ansible-compose`
  - start docker-compose: `docker-compose up -d`
  - to connect to control node: `docker-compose exec control /bin/bash`
  - you're all set-up. enjoy your class . :smiley:

> Note: docker images will need to built on you system, thus internet access is required